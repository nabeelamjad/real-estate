<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\GeneralSetting;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'username' => 'odcs',
            'email' => 'admin@admin.com',
            'password' => Hash::make(12345678),
            'is_master' => 1
        ]);


        GeneralSetting::create([
            'site_logo' => 'k6Xx5HT4kFXALAn.png',
            'favicon_icon' => 'k6Xx5HT4kFXALAn.png',
            'site_name' => 'Novel Feast'
        ]);
    }
}
