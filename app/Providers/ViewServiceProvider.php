<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\GeneralSetting;
use Illuminate\Support\Facades\Auth;
use App\Service;
class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.*', 'admin.*', '*'], function ($view) {
            $settings = GeneralSetting::first();
            $parentServices = Service::latest()->get();
            $view->with(compact('settings', 'parentServices'));
        });
    }
}
