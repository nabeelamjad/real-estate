<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Project;
use App\City;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::latest()->get();
        return view('admin.projects.index', compact('projects'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255', 'unique:projects'],
            'address' => ['required', 'string'],
            'location' => ['required', 'string'],
            'description' => ['required', 'string'],
            'featured_image' => ['required', 'image'],
            'body_images' => ['required', 'array'],
            'body_images.*' => ['image'],
            'application_form' => ['nullable', 'array'],
            'application_form.*' => ['image'],
            'meta_title' => ['nullable', 'string', 'max:255'],
            'meta_keywords' => ['nullable', 'string'],
            'meta_description' => ['nullable', 'string'],
        ]);

        $featured_image = $request->file('featured_image');
        $filename = Str::random(15) . '.' . $featured_image->extension();
        Storage::putFileAs("public", $featured_image, $filename);
        $data['featured_image'] = $filename;

        if (is_array($request->file('application_form'))) {
            $application_form = $request->file('application_form');
            $imageArray = array();
            foreach ($application_form as $key => $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                $imageArray[] = $filename;
            }
            $data['application_form'] = implode(',', $imageArray);
        }
        if (is_array($request->file('body_images'))) {
            $body_images = $request->file('body_images');
            $imageArray = array();
            foreach ($body_images as $key => $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                $imageArray[] = $filename;
            }
        }

        $data['body_images'] = implode(',', $imageArray);
        $data['slug'] = Str::slug($data['title'], '-');
        Project::Create($data);

        return redirect()->route('projects.index')->with('success', 'Project created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Project $project)
    {
        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Project $project)
    {
        $data = $request->validate([
            'title' => ['required', 'string', 'max:255', 'unique:projects,title,' . $project->id],
            'address' => ['required', 'string'],
            'location' => ['required', 'string'],
            'description' => ['required', 'string'],
            'featured_image' => ['nullable', 'image'],
            'body_images' => ['nullable', 'array'],
            'body_images.*' => ['image'],
            'application_form' => ['nullable', 'array'],
            'application_form.*' => ['mimes:pdf,docx'],
            'meta_title' => ['nullable', 'string', 'max:255'],
            'meta_keywords' => ['nullable', 'string'],
            'meta_description' => ['nullable', 'string'],
        ]);
        $data['slug'] = Str::slug($data['title'], '-');
        if ($request->file('featured_image')) {
            $featured_image = $request->file('featured_image');
            $filename = Str::random(15) . '.' . $featured_image->extension();
            Storage::putFileAs("public", $featured_image, $filename);
            $data['featured_image'] = $filename;
        }

        if (is_array($request->file('application_form'))) {
            $application_form = $request->file('application_form');
            $imageArray = array();
            foreach ($application_form as $key => $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                $imageArray[] = $filename;
            }
            $data['application_form'] = $project->application_form . ',' . implode(',', $imageArray);
        }

        if (is_array($request->file('body_images'))) {
            $body_images = $request->file('body_images');
            $imageArray = array();
            foreach ($body_images as $key => $image) {
                $filename = Str::random(15) . '.' . $image->extension();
                Storage::putFileAs("public", $image, $filename);
                $imageArray[] = $filename;
            }
            $data['body_images'] = $project->body_images . ',' . implode(',', $imageArray);
        } else {
            $data['body_images'] = $project->body_images;
        }

        $project->Update($data);
        return redirect()->route('projects.index')->with('success', 'Project updated successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->back()->with('success', 'Project deleted successfully.');
    }

    public function deleteImage(Project $project, $name)
    {
        $array = array();
        foreach (explode(',', $project->body_images) as $image) {
            if($image){
                if ($image == $name) {
                    Storage::delete($name);
                } else {
                    $array[] = $image;
                }
            }
        }
        $project->update([
            'body_images' => implode(',', $array)
        ]);
        return redirect()->back()->with('success', 'Body Image deleted successfully.');
    }


    public function deleteForm(Project $project, $name)
    {
        $array = array();
        foreach (explode(',', $project->application_form) as $form) {
            if ($form == $name) {
                Storage::delete($name);
            } else {
                $array[] = $form;
            }
        }
        $project->update([
            'application_form' => implode(',', $array)
        ]);
        return redirect()->back()->with('success', 'Application Form Document deleted successfully.');
    }
}
