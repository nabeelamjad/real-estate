<?php

namespace App\Http\Controllers\Frontend;

use App\Blog;
use App\Http\Controllers\Controller;
use App\ContactEnquiry;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Service;
use App\Testimonial;
use App\Career;
use App\Job;
use App\Project;
use App\Query;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Slider;

class PageController extends Controller
{
    public function index()
    {
        $services = Service::select('slug', 'title', 'description', 'featured_image')->latest()->take(6)->get();
        $testimonials = Testimonial::select('name', 'message', 'image')->latest()->get();
        $projectsList = Project::latest()->take(6)->get();
        $sliders = Slider::all();
        return view('frontend.pages.home', compact('services', 'testimonials', 'projectsList', 'sliders'));
    }
    public function about()
    {
        return view('frontend.pages.about');
    }

    public function service()
    {
        $services = Service::latest()->get();
        $servicesTwo = $services->take(2);
        $servicesThree = Service::whereNotIn('id', $servicesTwo->pluck('id'))->take(3)->get();
        return view('frontend.pages.services', compact('services', 'servicesThree', 'servicesTwo'));
    }

    public function serviceDetail($id){
        $service = Service::find($id);
        return response()->json($service);
    }

    public function blog()
    {
        $blogs = Blog::latest()->get();
        return view('frontend.pages.blogs', compact('blogs'));
    }

    public function blogDetails(Blog $blog)
    {
        $relatedBlogs = Blog::where('id', '!=', $blog->id)->take(4)->get();
        return view('frontend.pages.blog-details', compact('blog', 'relatedBlogs'));
    }

    public function serviceDetails(Service $service)
    {
        return view('frontend.pages.service-details', compact('service'));
    }

    public function project()
    {
        $cityProjects = Project::latest()->get();
        return view('frontend.pages.projects', compact('cityProjects'));
    }

    public function projectDetails(Project $project)
    {
        $relatedProjects = Project::where('id', '!=', $project->id)->take(4)->get();
        return view('frontend.pages.project-details', compact('project', 'relatedProjects'));
    }

    public function careers()
    {
        $careers = Career::latest()->get();
        return view('frontend.pages.careers', compact('careers'));
    }

    public function careerJob(Career $career)
    {
        return view('frontend.pages.jobs', compact('career'));
    }

    public function careerJobApply(Request $request, Career $career)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'cv' => ['required', 'file', 'mimes:pdf,docx'],
        ]);
        $data['career_id'] = $career->id;

        $cv = $request->file('cv');
        $filename = Str::random(15) . '.' . $cv->extension();
        Storage::putFileAs("public", $cv, $filename);
        $data['cv'] = $filename;


        Job::create($data);
        $request->session()->flash('success', 'Your Request Submit successfully. Thankyou for choosing Orion');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function careerJobTeam(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'cv' => ['required', 'file', 'mimes:pdf,docx'],
        ]);
        $cv = $request->file('cv');
        $filename = Str::random(15) . '.' . $cv->extension();
        Storage::putFileAs("public", $cv, $filename);
        $data['cv'] = $filename;


        Job::create($data);
        $request->session()->flash('success', 'Your Request Submit successfully. Thankyou for choosing Orion');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }


    public function contactUs()
    {
        return view('frontend.pages.contact-us');
    }

    public function contactEnquiry(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'message' => ['nullable', 'string']
        ]);
        Query::create($data);

        Mail::to('info@odcs.com.pk')->send(new ContactMail($data['name'], $data['email'], $data['phone'], $data['message']));

        $request->session()->flash('success', 'Your Request Submit successfully');
        $request->session()->flash('alert-class', 'alert alert-success');
        return redirect()->back();
    }

    public function contactSubmit(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'message' => ['nullable', 'string']
        ]);
        Query::create($data);

        Mail::to('info@odcs.com.pk')->send(new ContactMail($data['name'], $data['email'], $data['phone'], $data['message']));

        return response()->json([
            'message' => "Thank you your request has been submitted",
            'error' => false
        ]);
    }
}
