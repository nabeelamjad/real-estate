<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @yield('head')
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="Orion" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/logo/logo-black.png') }}" />
    <!-- font -->

    <!-- Plugins -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/plugins-css.css') }}" />

    <!-- revoluation -->
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}" media="screen" />

    <!-- Typography -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/typography.css') }}" />

    <!-- Shortcodes -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/shortcodes/shortcodes.css') }}" />

    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />

    <style>
        .mega-menu .menu-logo{
            padding:10px 0 10px
        }
        .quote:before {
            color: #fff;
        }
        .career-description span{
            color:white !important;
        }
        .bg-overlay-black-60:before{
            background: rgba(0, 0, 0, 0.2);
        }

        .blog-description p span,
        .blog-description p p,
        .blog-description p,
        .blog-description ul,
        .blog-description ul li,
        .blog-description ul li a.
        .blog-description h1,
        .blog-description h2,
        .blog-description h3,
        .blog-description h4,
        .blog-description h5,
        .blog-description h6 {
            color: white !important;
        }

        .blog-description p span{
            color: white !important;
        }

        .blog-description p{
            color: white !important;
        }
        /* .mega-menu .menu-logo img{
            height:70px
        } */
        @media screen and (max-width: 767.99px) {
            .header-image img {
                width: 360px
            }
            .page-section-ptb {
                padding: 20px 0 !important;
            }
            .hexagon-text{
                font-size: 12px;
                margin-bottom: 0px;
                white-space: break-spaces;
                padding: 17px;
            }
            .hexagon{
                height: 20vw !important;
            }
        }
        @media screen and (min-width: 768px) {
            .header-image img {
                width: 1519px
            }
            .hexagon-text{
                padding: 100px;
                font-size: 20px;
                margin-bottom: 0px;
                white-space: break-spaces;
            }
        }
        .business .box-newsletter{
            padding: 50px;
            background: rgba(38, 38, 38, 0.82);
        }
        .portfolio-item .portfolio-overlay{
            bottom: 0px !important;
            padding:10px !important;
        }
    </style>
</head>

<body>

    <div class="wrapper">
        <div id="pre-loader">
            <img src="{{ asset('images/pre-loader/loader-04.svg') }}" alt="Orion" />
        </div>
        @include('frontend.partials.header')
        @yield('content')
        <div class="elfsight-app-e0c7000e-1b6b-4f0a-a18e-cb9c1034f908"></div>
        @include('frontend.partials.footer')
    </div>

    <!-- Messenger Chat Plugin Code -->
    <div id="fb-root"></div>

    <!-- Your Chat Plugin code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "105776278690099");
      chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v13.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Messenger Chat Plugin Code -->
    <div id="back-to-top">
        <a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a>
    </div>

    <!-- jquery -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>

    <!-- plugins-jquery -->
    <script src="{{ asset('js/plugins-jquery.js') }}"></script>

    <!-- REVOLUTION JS FILES -->
    {{-- <script src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script> --}}

    <!-- revolution custom -->
    {{-- <script src="{{ asset('revolution/js/revolution-custom.js') }}"></script> --}}
    <!-- plugin_path -->

    <script>
        var plugin_path = window.location.pathname.split("/")[1] === "staging"
                            ? "/staging/js/"
                            : "" + "/js/";
    </script>


    <!-- REVOLUTION JS FILES -->
    <script src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- revolution custom -->
    <script src="{{ asset('revolution/js/revolution-custom.js') }}"></script>

    <!-- custom -->
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    <!--Start of Tawk.to Script-->
    {{-- <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/6210a3e1a34c245641271401/1fs8g0am8';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script> --}}
    <!--End of Tawk.to Script-->
    {{-- <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+923348969000", // WhatsApp number
                greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.",
                call_to_action: "Message us", // Call to action
                button_color: "#129BF4", // Color of button
                position: "left", // Position may be 'right' or 'left'
                order: "whatsapp", // Order of buttons
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script> --}}
    @stack('scripts')
</body>

</html>
