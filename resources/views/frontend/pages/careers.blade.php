@extends('frontend.layouts.layout')

@section('head')
<title>Careers | Orion</title>
<meta name="description" content="Careers | Orion">
<meta name="keywords" content="Careers | Orion">
@endsection

@section('content')
<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
  style="background-image: url({{ asset('images/jason-goodman-Oalh2MojUuk-unsplash.jpg') }});">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-title-name">
          <h1>Careers</h1>
        </div>
        <ul class="page-breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
          <li><span>Careers</span> </li>
        </ul>
      </div>
    </div>
  </div>
</section>
@if ($careers->count())
<section class="blog gray-bg page-section-ptb">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12 text-center" style="background: #1E64B0;
            padding: 10px;">
                <h1 class="text-white">Career Jobs</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($careers as $career)
            <div class="col-lg-4 align-self-center" style="background:#1E64B0;padding:20px">
                <div class="box-newsletter career-description">
                <h5 class="text-white" style="font-size:30px">{{ $career->title }}</h5>
                <p class="text-white mb-30">{!! Str::of($career->description)->words(20, '...') !!}</p>
                <div class="awsm-job-specification-wrapper">
                    <div class="awsm-job-specification-item awsm-job-specification-job-type">
                    <i class="fa fa-suitcase" style="color: #f5793b"></i>
                    <span class="awsm-job-specification-term text-white">{{ $career->job_type }}</span>
                    </div>
                    <div class="awsm-job-specification-item awsm-job-specification-job-location">
                    <i class="fa fa-map-marker" style="color:#f5793b"></i>
                    <span class="awsm-job-specification-term text-white">{{ $career->location }}</span>
                    </div>
                </div>
                <div class="mt-3">
                    <a class="button" style="background:#f5793b;color:white;border: 2px solid #f5793b;" href="{{ route('pages.careers-job', $career->slug) }}">More Details</a>
                </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@else
<section class="page-section-ptb" style="padding:10px 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="box-shadow: 0 0 3px 0 rgb(0 0 0 / 30%);
            padding: 35px;">
                <div class="section-title">
                    @include('common.partials.flash')
                    <h4 class="theme-color mb-3">Join us with our team</h4>
                    <form action="{{ route('pages.careers-job-team') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-12 mt-3">
                            <label>Full Name <span style="color:red"> *</span></label>
                            <input type="text" class="form-control" name="name" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Email <span style="color:red"> *</span></label>
                            <input type="email" class="form-control" name="email" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Phone <span style="color:red"> *</span></label>
                            <input type="text" class="form-control" name="phone" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>City <span style="color:red"> *</span></label>
                            <select class="form-control" name="city">
                                <option value="">Select City</option>
                                <option value="lahore">Lahore</option>
                                <option value="karachi">Karachi</option>
                                <option value="islamabad">Islamabad</option>

                            </select>
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Upload CV/Resume <span style="color:red"> *</span></label>
                            <input type="file" class="form-control" accept=".pdf,.docx" name="cv" />
                        </div>
                        <div class="col-md-12 mt-3 text-center">
                            <input type="submit" class="button button-gradient" value="submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection
