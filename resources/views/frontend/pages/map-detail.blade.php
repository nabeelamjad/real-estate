@extends('frontend.layouts.layout')

@section('head')
    <title>Maps</title>
    <meta name="description" content="Maps">
    <meta name="keywords" content="Maps">
    <style>
        *body {
            margin: 0;
        }

        .main {
            width: 100%;
            height: 70vh;
            overflow: hidden;
            cursor: grab;
            cursor: -o-grab;
            cursor: -moz-grab;
            cursor: -webkit-grab;
        }

        .main img {
            height: auto;
            width: 100%;
            padding:20px
        }

        .button {
            width: 300px;
            height: 60px;
        }

        #exit_full {
            display: none;
        }

        .imageview {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            background-color: #EDEDED;
        }

        .imgaeview-controller {}

        .imgaeview-controller a {
            color: #f7f7f7;
        }

        input[type="range"] {
            -webkit-appearance: none;
            outline: none !important;
            appearance: none;
            border: none;
            border-radius: 30px;
        }

        input[type="range"]::-moz-focus-outer {
            border: 0;
        }

        input[type="range"]:hover {
            outline: none;
        }
        /* Chrome */

        input[type="range"]::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 9px;
            height: 9px;
            background: #000;
            cursor: pointer;
            border-radius: 30px;
            outline: none;
        }
        /* Moz */

        input[type="range"]::-moz-range-thumb {
            width: 18px;
            height: 18px;
            background: #e3e3e3 !important;
            cursor: pointer;
            border-radius: 50%;
            border: none;
            outline: none;
        }

        input[type="range"]::-moz-range-progress {
            background-color: #e3e3e3 !important;
            height: 100%;
            border-radius: 30px;
            border: none;
        }

        input[type="range"]::-moz-range-track {
            background-color: #e3e3e3 !important;
            border-radius: 30px;
            border: none;
            height: 100%;
        }
        /* IE*/

        input[type="range"]::-ms-fill-lower {
            background-color: #e3e3e3 !important;
            height: 100%;
            border-radius: 30px;
            border: none;
        }

        input[type="range"]::-ms-fill-upper {
            background-color: #e3e3e3 !important;
            border-radius: 30px;
            border: none;
            height: 100%;
        }
        .button-view{
            color: #9a9a9a;
            font-size: 17px !important;
        }
        @media (max-width: 767.99px) {
            #slide{
                display: none;
            }
            #exit_text{
                display:none;
            }
            .px-2{
                padding:0px !important;
            }
            .p0{
                padding:0 2px !important;
            }
            .main{
                height: 45vh;
            }
            .page-section-ptb{
                padding:10px !important;
            }
        }
        .dragscroll {
            cursor : -webkit-grab;
            cursor : -moz-grab;
            cursor : -o-grab;
            cursor : grab;
            color: #fff;
            cursor: grab !important;
            overflow: auto;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@endsection

@section('content')
<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url({{ asset('images/login.jpg') }});">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
        <div class="page-title-name">
            <h1>Map Details</h1>
            <p>We know the secret of your success</p>
          </div>
            <ul class="page-breadcrumb">
              <li><a href="{{ route('pages.home') }}"><i class="button-view fa fa-home"></i> Home</a> <i class="button-view fa fa-angle-double-right"></i></li>
              <li><a href="{{ route('pages.maps') }}"><i class="button-view fa fa-home"></i> Maps</a> <i class="button-view fa fa-angle-double-right"></i></li>
              <li><span>{{ $map->title }}</span> </li>
         </ul>
       </div>
       </div>
    </div>
</section>

<section class="blog white-bg page-section-ptb" style="padding:60px;background:#f7f7f7">
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color fw-5">{{ $map->title }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix" id="img-container">
                        <img id="img" class="img-fluid" src="{{ asset('/storage/' . $map->map) }}" alt="">
                    </div>
                </div>
                <input type="range" min="1" max="4" value="1" step="0.1" id="zoomer" oninput="deepdive()">
            </div>
        </div>
    </div> --}}
    <div class="container ">
        <div class="row" style="background:white;">
            <div class="col-md-2 col-lg-2">
                <h2 class="theme-color fw-5" style="padding: 10px 0px;border-bottom:5px solid green;white-space: pre;">{{ $map->title }}</h2>
            </div>
        </div>
        <div class="row" style="background:#EDEDED">
            <div class="col-md-3" style="padding:20px;">
                <img src="{{ asset('/storage/' . $map->map) }}" style="height:140px;border:2px solid #000;padding:5px" />
            </div>
            <div class="col-md-9 p-0" style="border:4px solid #000">
                <div class="imageview">
                    <div class="main dragscroll" id="main">
                        <img id="map" src="{{ asset('/storage/' . $map->map) }}" />
                    </div>
                    <div class='imgaeview-controller py-2' style="width: 100%;
                    text-align: center;
                    background: #EAEAEA;">
                        <div class="row container">
                            <div class="col-md-4 col-3 p0" style="border-right: 3px solid #919191;
                            text-align: center;">
                                <a href="javascript:zoomout();" class="px-1"><i class="button-view fa fa-minus"></i></a>
                                <input style="background-color: #9a9a9a;font-size: 20px;" type="range" min="0" max="15" step="1" value="0" oninput="onInput(this.value)" id="slide">
                                <a href="javascript:zoomin();" class="px-1"><i class="button-view fa fa-plus"></i></a>
                            </div>
                            <div class="col-md-4 col-7 p0" style="border-right: 3px solid #919191;
                            text-align: center;">
                                <a href="javascript:scrollUp();" class="px-1"><i class="button-view fa fa-arrow-up"></i></a>
                                <a href="javascript:ScrollDown();" class="px-1"><i class="button-view fa fa-arrow-down"></i></a>
                                <a href="javascript:ScrollLeft();" class="px-1"><i class="button-view fa fa-arrow-left"></i></a>
                                <a href="javascript:ScrollRight();" class="px-1"><i class="button-view fa fa-arrow-right"></i></a>
                                <a href="javascript:resetImage();" class="px-1"><i class="button-view fa fa-undo"></i></a>
                            </div>
                            <div class="col-md-4 col-1" style="text-align:center">
                                <a href="javascript:viewFullScreen();" class="px-1" id="view_full"><i class="button-view fa fa-arrows"></i></a>
                                <a href="javascript:exitFullScreen();" class="px-1" id="exit_full"><i class="button-view fa fa-times-circle"></i></a><span id="exit_text"> Full Page Review</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@if (count($relatedMaps) > 0)
    <section class="blog white-bg page-section-ptb">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-12 col-lg-12">
                    <h2 class="theme-color fw-5">Related Posts</h2>
                </div>
            </div>
            <div class="row">
                @foreach ($relatedMaps as $relatedMap)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog-entry mb-50">
                            <div class="entry-image clearfix">
                                <img class="img-fluid" src="{{ asset('/storage/' . $relatedMap->map) }}" alt="">
                            </div>
                            <div class="blog-detail">
                                <div class="entry-title mb-10">
                                    <a href="{{ route('pages.map-detail', $relatedMap->id) }}">{{ $relatedMap->title }}</a>
                                </div>
                                <div class="entry-share clearfix">
                                    <div class="entry-button">
                                        <a class="button arrow text-left" href="{{ route('pages.map-detail', $relatedMap->id) }}">Read More<i class="button-view fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
@endsection
@push('scripts')
<script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>
<script>
    var myImg = document.getElementById("map");
    var actualWidth = myImg.clientWidth;
    var range_step = 1;
    var slide_val;
    var minValue = $('#slide').prop('min');
    var maxValue = $('#slide').prop('max');
    $(document).ready(function() {


    });

    function resetImage() {
        myImg.style.width = actualWidth + "px";
        $("#slide").val(0);
        $("#slide").trigger('change');
    }

    function viewFullScreen() {
        var windowHeight = $(window).height();
        $(".main").height(windowHeight);
        // $(".main").css('height', 'auto');
        $(".main").width('100%');
        $('#exit_full').show();
        $('#view_full').hide();
        $('#exit_text').html('Exit Full View');
    }

    function exitFullScreen() {

        var windowHeight = $(window).height() / 2;
        $(".main").height(windowHeight);
        $(".main").css('height', 'auto');

        $(".main").width('100%');
        $('#exit_full').hide();
        $('#view_full').show();
        $('#exit_text').html('Full Page Review');

    }

    function zoomin() {
        slide_val = parseInt($("#slide").val());
        console.log(slide_val);
        console.log(maxValue);
        if (slide_val < maxValue) {

            $("#slide").val(slide_val + range_step);
            $("#slide").trigger('change');
            callIn();
        }
    }

    function callIn() {
        var currWidth = myImg.clientWidth;
        if (currWidth == 2500) return false;
        else {
            myImg.style.width = (currWidth + 100) + "px";
        }
    }

    function callOut() {
        var currWidth = myImg.clientWidth;
        if (currWidth == 100) return false;
        else {
            myImg.style.width = (currWidth - 100) + "px";
        }

    }

    function zoomout() {

        slide_val = parseInt($("#slide").val());
        if (slide_val > minValue) {
            $("#slide").val(slide_val - range_step);
            $("#slide").trigger('change');
            callOut();
        }

    }

    function scrollUp() {
        document.getElementById('main').scrollTop -= 10;
    }


    function ScrollDown() {
        document.getElementById('main').scrollTop += 10;
    }


    function ScrollLeft() {
        document.getElementById('main').scrollLeft -= 10;
    }

    function ScrollRight() {
        document.getElementById('main').scrollLeft += 10;
    }

    var lastNum = 0;

    function onInput(value) {
        if (lastNum < value) {
            callIn();
        } else {
            callOut();
        }
        lastNum = value;
    }
</script>
@endpush
