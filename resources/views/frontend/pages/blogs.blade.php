@extends('frontend.layouts.layout')

@section('head')
<title>Blogs | Orion</title>
<meta name="description" content="Blogs | Orion">
<meta name="keywords" content="Blogs | Orion">
@endsection

@section('content')
<section class="header-image bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'>
    <img src="{{ asset('images/blog-header.jpg') }}" />
</section>

@if (count($blogs) > 0)
<section class="blog gray-bg page-section-ptb">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12 text-center" style="background: #1E64B0;
            padding: 10px;">
                <h1 class="text-white">Our Blogs</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($blogs as $blog)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-entry mb-50 slideInUp wow" style="box-shadow: 0px 0px 30px rgb(0 0 0 / 10%);">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $blog->featured_image) }}" alt="">
                    </div>
                    <div class="blog-detail bg-primary">
                        <div class="entry-title mb-10">
                            <a class="text-white" href="{{ route('pages.blog-details', $blog->slug) }}">{{ $blog->title }}</a>
                        </div>
                        <div class="entry-meta mb-10">
                            <ul>
                                <li> <i class="fa fa-map" style="color: #f5793b"></i> <a class="text-white" href="{{ route('pages.blog-details', $blog->slug) }}">
                                        {{ $blog->location }}</a> </li>
                                <li><a class="text-white" href="{{ route('pages.blog-details', $blog->slug) }}"><i
                                            class="fa fa-calendar-o" style="color: #f5793b"></i> {{ date('d M Y',
                                        strtotime($blog->published_date)) }}</a></li>
                            </ul>
                        </div>
                        <div class="entry-title blog-description mb-10">
                            {!! Str::of($blog->description)->words(20, '...') !!}
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button button-border white" style="background:#f5793b;color:white;border: 2px solid #f5793b;" href="{{ route('pages.blog-details', $blog->slug) }}">Read More</a>
                                {{-- <a class="button" href="{{ route('pages.blog-details', $blog->slug) }}">Read More</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@endsection
