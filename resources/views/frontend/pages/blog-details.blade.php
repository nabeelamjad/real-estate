@extends('frontend.layouts.layout')

@section('head')
    <title>{{ $blog->meta_title }}</title>
    <meta name="description" content="{{ $blog->meta_description }}">
    <meta name="keywords" content="{{ $blog->meta_keywords }}">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ul class="page-breadcrumb">
                <li style="color:#1E64B0"><a style="color:#1E64B0" href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                        class="fa fa-angle-double-right"></i></li>
                <li style="color:#000000"><a style="color:#000000" href="{{ route('pages.blogs') }}">
                        Blogs</a></li>
            </ul>
        </div>
    </div>
</div>
<section class="blog white-bg page-section-ptb" style="padding:10px 0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color fw-5">{{ $blog->title }}</h2>
                <p>{{ date('d M Y', strtotime($blog->published_date)) }} | {{ $blog->location }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $blog->body_image) }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12">
                <p>{!! $blog->description !!}
                </p>
            </div>
        </div>
    </div>
</section>

@if (count($relatedBlogs) > 0)
    <section class="blog white-bg page-section-ptb">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-12 col-lg-12">
                    <h2 class="theme-color fw-5">Related Posts</h2>
                </div>
            </div>
            <div class="row">
                @foreach ($relatedBlogs as $relatedBlog)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog-entry mb-50">
                            <div class="entry-image clearfix">
                                <img class="img-fluid" src="{{ asset('/storage/' . $relatedBlog->featured_image) }}" alt="">
                            </div>
                            <div class="blog-detail">
                                <div class="entry-title mb-10">
                                    <a href="{{ route('pages.blog-details', $relatedBlog->slug) }}">{{ $relatedBlog->title }}</a>
                                </div>
                                <div class="entry-meta mb-10">
                                    <ul>
                                        <li> <i class="fa fa-map"></i> <a href="{{ route('pages.blog-details', $relatedBlog->slug) }}"> {{ $relatedBlog->location }}</a> </li>
                                        <li><a href="{{ route('pages.blog-details', $relatedBlog->slug) }}"><i class="fa fa-calendar-o"></i> {{ date('d M Y', strtotime($relatedBlog->published_date)) }}</a></li>
                                    </ul>
                                </div>
                                <div class="entry-share clearfix">
                                    <div class="entry-button">
                                        <a class="button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
@endsection
