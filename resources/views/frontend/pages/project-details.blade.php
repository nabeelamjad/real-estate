@extends('frontend.layouts.layout')

@section('head')
<title>{{ $project->meta_title }}</title>
<meta name="description" content="{{ $project->meta_description }}">
<meta name="keywords" content="{{ $project->meta_keywords }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ul class="page-breadcrumb">
                <li style="color:#1E64B0"><a style="color:#1E64B0" href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                        class="fa fa-angle-double-right"></i></li>
                <li style="color:#000000"><a style="color:#000000" href="{{ route('pages.projects') }}">
                        Projects</a></li>
            </ul>
        </div>
    </div>
</div>
<section class="shop-single page-section-ptb" style="padding:10px 0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color">{{ $project->title }}</h2>
                <p>{{ $project->address }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        {{-- <div class="slider-slick"> --}}
                            {{-- <div class="slider slider-for detail-big-car-gallery mb-3">
                                @foreach (explode(',', $project->body_images) as $bodyImage)
                                <img style="height:430px" class="img-fluid" src="{{ asset('/storage/' . $bodyImage) }}"
                                    alt="">
                                @endforeach
                            </div> --}}
                            <div class="full-width">
                                <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1"
                                    data-xx-items="1" data-space="20">
                                    @foreach (explode(',', $project->body_images) as $bodyImage)
                                        <div class="item">
                                            <img style="height:430px" class="img-fluid full-width" src="{{ asset('/storage/' . $bodyImage) }}"
                                            alt="">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            {{-- <div class="slider slider-nav">
                                @foreach (explode(',', $project->body_images) as $bodyImageThumbnail)
                                <img style="height:300px" class="img-fluid mr-2"
                                    src="{{ asset('/storage/' . $bodyImageThumbnail) }}" alt="">
                                @endforeach

                            </div> --}}
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                {!! $project->description !!}
            </div>
        </div>
        @if ($project->application_form)
        <div class="row mt-3">
            @foreach (explode(',', $project->application_form) as $form)
            <div class="col-md-6 col-lg-6">
                <div style="display:flex">
                    <h4 style="margin-top:10px">Application Form:</h4>
                    <a href="{{ asset('/storage/' . $form) }}" target="_blank"
                        class="ml-4 btn btn-success">View/Download</a>
                </div>
            </div>
            @endforeach
        </div>
        @endif
        <div class="row mt-3">
            <div class="col-md-12 col-lg-12 text-center">
                {!! $project->location !!}
            </div>
        </div>
    </div>
</section>

@if (count($relatedProjects) > 0)
<section class="page-section-ptb">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12">
                <h1 class="theme-color">Related Projects</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="isotope popup-gallery columns-3">
                    @foreach ($relatedProjects as $relatedProject)
                    <div class="grid-item">
                        <div class="portfolio-item">
                            <img src="{{ asset('/storage/' . $relatedProject->featured_image) }}" alt="" style="height:270px">
                            <div class="portfolio-overlay">
                                <h4 class="text-white"> <a
                                        href="{{ route('pages.project-details', $relatedProject->slug) }}">{{ $relatedProject->title }}</a>
                                </h4>
                                <span class="text-white"> <a
                                        href="{{ route('pages.project-details', $relatedProject->slug) }}">
                                        {{ $relatedProject->address }}</span>
                            </div>
                            <a class="popup portfolio-img"
                                href="{{ asset('/storage/' . $relatedProject->featured_image) }}"><i
                                    class="fa fa-arrows-alt"></i></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@endsection
