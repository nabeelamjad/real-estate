@extends('frontend.layouts.layout')

@section('head')
    <title>Contact us | Orion</title>
    <meta name="description" content="Orion">
    <meta name="keywords" content="Orion">
@endsection

@section('content')
<section class="header-image bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'>
    <img src="{{ asset('images/contact-header.png') }}" />

    {{-- <div class="container">
    <div class="row">
      <div class="col-lg-12">
      <div class="page-title-name">
          <h1>Contact Us</h1>
        </div>
          <ul class="page-breadcrumb">
            <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
            <li><span>Contact us</span> </li>
       </ul>
     </div>
   </div>
  </div> --}}
  <div class="overlay"></div>
</section>
<section class="page-section-ptb">
    <div class="container-fluid">
        <div class="row slideInRight wow">
            <div class="col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3403.1119931233293!2d74.45136331499442!3d31.466105281387613!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3919092010948db7%3A0x83afaef84719c1cb!2sPlot%2023%2C%20Sector%20D%20DHA%20Phase%206%2C%20Lahore%2C%20Punjab%2C%20Pakistan!5e0!3m2!1sen!2s!4v1631992549927!5m2!1sen!2s" style="width: inherit;" height="458" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                <div class="container mt-3">
                    <h2 class="text-center">Location Details</h2>
                    <ul class="addresss-info">
                        <li><i class="fa fa-map-marker text-black" style="color:black"></i> <p class="text-black" style="color:black">Corporate Office: 11G, Commercial, Main Boulevard, DHA Phase 1, Lahore, Pakistan</p> </li>
                        <li><i class="fa fa-map-marker text-black" style="color:black"></i> <p class="text-black" style="color:black">Regional Office: 160 Kemp House, City Road London ECIV 2NX, United Kingdom</p> </li>
                        <li><i class="fa fa-phone text-black" style="color:black"></i> <a href="tel:923348969000"> <span class="text-black" style="color:black">+(92) 334 8969 000</span> </a> </li>
                        <li><i class="fa fa-phone text-black" style="color:black"></i> <a href="tel:447862073715"> <span class="text-black" style="color:black">+(44) 786 2073 715</span> </a> </li>
                        <li class="text-black" style="color:black"><i class="fa fa-envelope-o text-black" style="color:black"></i>Email: <a class="text-black" href="mailto:info@odcs.com.pk" style="color:black">info@odcs.com.pk</a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3402.516150678098!2d74.39161361476732!3d31.482493581382123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391905e28b54e757%3A0x8d3d3e1f994e211f!2sD.H.A.%20Main%20Blvd%2C%20Lahore%2C%20Punjab%2C%20Pakistan!5e0!3m2!1sen!2s!4v1648067068503!5m2!1sen!2s" style="width: inherit;" height="458" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                <div class="container mt-3">
                    <h2 class="text-center">Opening Hours</h2>
                    <ul class="addresss-info">
                        <li><i class="fa fa-clock-o text-black" style="color:black"></i> <p class="text-black" style="color:black">Monday - Friday: 9am to 6pm</p> </li>
                        <li><i class="fa fa-clock-o text-black" style="color:black"></i> <p class="text-black" style="color:black">Saturday - Sunday: 10am to 4pm</p> </li>
                        <li>
                            <ul>
                                <li class="social-facebook"><a href="https://www.facebook.com/oriondcs"><i style="color:black" class="fa fa-facebook"></i></a></li>
                                <li class="social-linkedin"><a href="https://www.linkedin.com/company/oriondcs"><i style="color:black" class="fa fa-linkedin"></i> </a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-section-ptb clearfix o-hidden" style="padding:50px 0px;    background: #1E64B0;
color: white;">
    <div class="container">
      <div class="row justify-content-end">
        <div class="contact-4 col-lg-6 sm-mb-30">
          <blockquote class="blockquote quote pl-0">
            Please connect with us and our support team will answer all your question.
          </blockquote>
          <p class="text-white">It would be great to hear from you! If you have any questions or need help, please fill out the form. We do our best to respond within 1 business day.</p>
          <p class="text-white"> We are looking forward to hearing from you please do not hesitate to send us a message.</p>
        </div>
        <div class="contact-3 col-lg-6">
            <div class="contact-3-info page-section-ptb" style="padding: 47px 0px;">
            {{-- @include('common.partials.flash') --}}
            <div id="success-message" class="alert {{ session('alert-class', 'alert-info') }} border-0 alert-dismissible" style="display: none">
                <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                Thank you your request has been submitted successfully!
            </div>
            <div class="clearfix slideInUp wow">
                <form  action="#" method="post" id="contact">
                    @csrf
                    <div class="contact-form clearfix">
                    <div class="section-field">
                        <input id="name" type="text" placeholder="Name*" class="form-control"  name="name" required>
                        </div>
                        <div class="section-field">
                        <input type="email" id="email" placeholder="Email*" class="form-control" name="email" required>
                        </div>
                        <div class="section-field">
                        <input type="tel" id="phone" placeholder="Phone*" class="form-control" name="phone" required>
                        </div>
                        <div class="section-field textarea">
                        <textarea class="input-message form-control" placeholder="Message" id="message" rows="7" name="message"></textarea>
                        </div>
                            <div class="button button-border white ml-0 d-none" id="loader" style="background: white;
                            padding: 0 70px;">
                                <img style="width: 42px;" src="{{ asset('images/pre-loader/loader-04.svg') }}" alt="Orion" />
                            </div>
                        <button style="background:#ffffff; color:#1E64B0" id="submit" name="submit" type="submit" value="Send" class="button button-border white d-inline-block ml-0"><span> Send message </span> <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@push('scripts')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        $('#contact').on('submit', function(e) {
            e.preventDefault();
            $('#loader').removeClass('d-none')
            $('#loader').addClass('d-inline-block')
            $('#submit').removeClass('d-inline-block')
            $('#submit').addClass('d-none')
            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var message = $('#message').val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'post',
                url: "/contact/form-submit",
                data:{
                    name:name,
                    phone: phone,
                    email: email,
                    message: message
                },
                dataType: 'json',
                success:function(data){
                    if(!data.error)
                    {
                        $('#loader').addClass('d-none')
                        $('#loader').removeClass('d-inline-block')
                        $('#submit').addClass('d-inline-block')
                        $('#submit').removeClass('d-none')
                        $('#success-message').attr('style', 'display:block');
                    }
                }
            });
        })
    </script>
@endpush
