@extends('frontend.layouts.layout')

@section('head')
<title>Homepage | Orion</title>
<meta name="description" content="Orion">
<meta name="keywords" content="Orion">
<style>
    /* .owl-dots{
        display:none !important;
    } */
</style>
@endsection

@section('content')
{{-- <section class="rev-slider">
    <div id="rev_slider_273_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="orion-slider-7" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
    <div id="rev_slider_273_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
  <ul>  <!-- SLIDE  -->
      <li data-index="rs-766" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{ asset('/images/s3.png') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
          <img src="{{ asset('/images/s3.png') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
    </li>
    <!-- SLIDE  -->
      <li data-index="rs-767" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="{{ asset('/images/s4.png') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
      <!-- MAIN IMAGE -->
          <img src="{{ asset('/images/s4.png') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
      <!-- LAYERS -->
    </li>
  </ul>
  <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
  </div>
</section> --}}
<div class="full-width">
    <div class="owl-carousel" data-nav-dots="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1"
        data-xx-items="1" data-space="20">
            <div class="item">
                <img class="img-fluid full-width" src="{{ asset('/images/s3.png') }}" alt="">
            </div>
            <div class="item">
                <img class="img-fluid full-width" src="{{ asset('/images/s4.png') }}" alt="">
            </div>
    </div>
</div>
<section class="page-section-ptb" style="background:rgba(51,81,124,1) 50%">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="section-title text-center">
                <h6 class="text-white">We're Good At</h6>
                <h2 class="font-weight-bold text-white"> Our services </h2>
                </div>
            </div>
          </div>
            <div class="row">
                @foreach ($services as $service)
                <div class="col-lg-4 col-md-4 col-sm-4 xs-mb-30 mb-5">
                    <div class="feature fadeInUp wow">
                        <div class="feature-image mb-20">
                            <img style="height:257px" alt="" src="{{ asset('/storage/' . $service->featured_image) }}" class="img-fluid">
                        </div>
                        <div class="feature-info service">
                        <h3 class="text-white">
                            <a href="{{ route('pages.service-details', $service->slug) }}"> {{ $service->title }} </a> </h3>
                            {!! Str::of($service->description)->words(20, '...') !!}
                            <a style="font-size:large;color:#f5793b" href="{{ route('pages.service-details', $service->slug) }}">Read More</a>
                        </div>
                    </div>
                </div>
             @endforeach
            </div>
        </div>
</section>

<section class="page-section-ptb">
    <div class="container">
         <div class="row">
            <div class="col-xl-6 xs-mt-30 xs-mb-30">
                <div class="owl-carousel" data-nav-arrow="true" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
                <div class="item"><img class="img-fluid full-width" src="{{ asset('images/slide1.jpg') }}" alt="">
                </div>
                <div class="item"><img class="img-fluid full-width" src="{{ asset('images/slide2.jpg') }}" alt="">
                </div>
                <div class="item"><img class="img-fluid full-width" src="{{ asset('images/slide3.jpg') }}" alt="">
                </div>
               </div>
            </div>
           <div class="col-lg-6">
              <div class="section-title mb-20">
              <h6>What we do?</h6>
              <h2>We do things differently</h2>
              <p>We are dedicated to providing you with the best experience possible.</p>
            </div>
             <p><span class="dropcap square">O</span>ur integrated approach to design and construction projects means we have highly professional team of architects, engineers and surveyors. This allows Orion Design and Construction Services to fulfil projects that span design, project management and building works.
                This approach enables us to value-engineer projects, assuring efficient project management and on-time completion while keeping our clients' needs at the forefront of our services.</p>
             <div class="action-box-button mt-30">
             <a class="button icon mb-10" target="_blank" href="https://m.me/oriondcs">
              <span>Book Appointment</span>
              <i class="fa fa-hand-o-right"></i>
           </a>
          </div>
        </div>
       </div>
    </div>
</section>
<section class="page-section-ptb theme-bg">
    <div class="container">
        <div class="row ">
         <div class="col-lg-4 col-md-4 col-sm-4 xs-mb-30">
           <div class="feature-text text-center fadeInUp wow">
             <div class="feature-icon">
             <span aria-hidden="true" class="ti-layers-alt text-white"></span>
             </div>
             <h4 class="text-white pt-20">About us</h4>
             <p class="text-white">Welcome to
                Orion Design & Contructions. Orion D&C is an International design and construction services provider that has intense experience in project management, multidisciplinary Engineering and construction services. </p>
           </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4 xs-mb-30">
           <div class="feature-text text-center fadeInUp wow">
             <div class="feature-icon">
             <span aria-hidden="true" class="ti-image text-white"></span>
             </div>
             <h4 class="text-white pt-20">Our Mission</h4>
             <p class="text-white">As the construction industry undergoes a digital transformation, Orion D&C is working to become a next-generation design and construction services firm and committed to provide exceptional value and demonstrable results to clients.</p>
           </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="feature-text text-center fadeInUp wow">
             <div class="feature-icon">
             <span aria-hidden="true" class="ti-heart text-white"></span>
             </div>
             <h4 class="text-white pt-20">Our Vision</h4>
             <p class="text-white">To be recognized as a world-class engineering and construction company.</p>
           </div>
         </div>
      </div>
    </div>
</section>


{{-- <section class="gray-bg page-section-pt happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-end">
                <img class="d-xs-block d-lg-block d-none img-fluid" src="images/objects/testimonial.png" alt="">
            </div>
            <div class="col-lg-6 mt-60">
                <div class="section-title">
                    <h6>Have a look at our</h6>
                    <h2 class="title-effect">Client Opinions & Reviews</h2>
                </div>
                <div class="tab">
                    <div class="tab-content" id="nav-tabContent">
                        @foreach ($testimonials as $key => $testimonial)
                            <div class="tab-pane fade show {{ $key === 0 ? 'active' : ''}}" id="testi-{{ $key }}">
                                <span class="quoter-icon">“</span>
                                <p>{{ $testimonial->message }}</p>
                                <div class="testimonial-avatar">
                                    <h5>{{ $testimonial->name }} </h5>
                                </div>
                            </div>
                        @endforeach
                        <ul class="nav nav-tabs mt-60" id="myTab" role="tablist">
                            @foreach ($testimonials as $key => $testimonial)
                                <li>
                                    <a class="nav-item {{ $key == 0 ? 'active' : ''}}" href="#testi-{{ $key }}" data-toggle="tab"><img class="img-fluid" src="{{ asset('/storage/' . $testimonial->image) }}" alt=""> </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<section>
    <div class="container-fluid p-0">
       <div class="row no-gutter">
           @forelse ($projectsList->take(3) as $key => $project)
                <div class="col-lg-4 feature-step-2 bg-01 bg-overlay-black-50 slideInLeft wow" style="background: url({{ asset('/storage/' . $project->featured_image) }})">
                    <div class="clearfix">
                    <div class="feature-step-2-box">
                        <div class="feature-step-2-title-left">
                        <h1>0{{ ++$key }}</h1>
                        </div>
                        <div class="feature-step-2-title-2-right blog-description">
                        <h3 class="text-white mb-3">{{ Str::of($project->title)->words(4, '...') }}</h3>
                        {!! Str::of($project->description)->words(30, '...') !!}
                        <a class="button button-border white" style="background:#f5793b;color:white;border: 2px solid #f5793b;" href="{{ route('pages.project-details', $project->slug) }}">Read More</a>
                        </div>
                        </div>
                        </div>
                </div>
           @empty
                <div class="w-100 text-center">
                    <h2>Projects Not Found</h2>
                </div>
           @endforelse
          </div>
    </div>
</section>

<section class="agency-form page-section-ptb" style="background:#1E64B0">
    <div class="container">
    {{-- @include('common.partials.flash') --}}
    <div id="success-message" class="alert {{ session('alert-class', 'alert-info') }} border-0 alert-dismissible" style="display: none">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
        Thank you your request has been submitted successfully!
    </div>
     <div class="row col-md-12 justify-content-center w-100">
      <div class="text-center fadeInDown wow">
         <div class="contact-form full-width clearfix">
           <div class="mb-30">
             <h3 class="text-white">Request Call Back</h3>
           </div>
           <form action="#" method="post" id="contact">
               @csrf
          <div class="section-field">
            <input id="name" type="text" id="name" placeholder="Name*" class="form-control placeholder" name="name">
           </div>
           <div class="section-field">
              <input type="email" id="email" placeholder="Email*" class="form-control placeholder" name="email">
            </div>
            <div class="section-field">
                <input type="tel" id="phone" placeholder="Contact Number*" class="form-control placeholder" name="phone">
            </div>
           <div class="section-field textarea">
             <textarea class="input-message form-control placeholder" placeholder="Message*" rows="4" id="message" name="message"></textarea>
            </div>
            <input type="hidden" name="action" value="sendEmail">
            <div class="button button-border white mt-20 d-none" id="loader" style="background: white;
            padding: 0 70px;">
                <img style="width: 42px;" src="{{ asset('images/pre-loader/loader-04.svg') }}" alt="Orion" />
            </div>
             <button id="submit" name="submit" type="submit" value="Send" class="button button-border white mt-20 d-inline-block"><span> Send message </span> <i class="fa fa-paper-plane"></i></button>
           </form>
          </div>
        </div>
      </div>
   </div>
</section>

@endsection
@push('scripts')
    <script>
        $('#contact').on('submit', function(e) {
            e.preventDefault();
            $('#loader').removeClass('d-none')
            $('#loader').addClass('d-inline-block')
            $('#submit').removeClass('d-inline-block')
            $('#submit').addClass('d-none')
            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var message = $('#message').val();
            var token = "{{ csrf_token() }}";
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': token
                },
                method: 'post',
                url: "/contact/form-submit",
                data:{
                    name:name,
                    phone: phone,
                    email: email,
                    message: message
                },
                dataType: 'json',
                success:function(data){
                    if(!data.error)
                    {
                        $('#loader').addClass('d-none')
                        $('#loader').removeClass('d-inline-block')
                        $('#submit').addClass('d-inline-block')
                        $('#submit').removeClass('d-none')
                        $('#success-message').attr('style', 'display:block');
                    }
                }
            });
        })
    </script>
@endpush
