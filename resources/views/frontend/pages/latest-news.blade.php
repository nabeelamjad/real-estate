@extends('frontend.layouts.layout')

@section('head')
<title>Latest News | Orion</title>
<meta name="description" content="Latest News | Orion">
<meta name="keywords" content="Latest News | Orion">
<style>
    #jarallax-container-0 div {
        background-position: 0% !important;
    }
</style>
@endsection

@section('content')
<section class="header-image bg-overlay-black-60">
    <img src="{{ asset('images/latest-news.jpg') }}" />
</section>
{{-- <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
    style="background-image: url({{ asset('images/latest-news.jpg') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>Latest News</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><span>Latest News</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section> --}}

@if (count($latestNews) > 0)
<section class="blog white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            @foreach ($latestNews as $latestNewsRecord)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $latestNewsRecord->featured_image) }}"
                            alt="">
                    </div>
                    <div class="blog-detail">
                        <div class="entry-title mb-10">
                            <a href="{{ route('pages.latest-news-details', $latestNewsRecord->slug) }}">{{
                                $latestNewsRecord->title }}</a>
                        </div>
                        <div class="entry-meta mb-10">
                            <ul>
                                <li><a href="{{ route('pages.latest-news-details', $latestNewsRecord->slug) }}"><i
                                            class="fa fa-calendar-o"></i> {{ date('d M Y',
                                        strtotime($latestNewsRecord->published_date)) }}</a></li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            {!! $latestNewsRecord->excerpt !!}
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button arrow"
                                    href="{{ route('pages.latest-news-details', $latestNewsRecord->slug) }}">Read More<i
                                        class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@endsection
