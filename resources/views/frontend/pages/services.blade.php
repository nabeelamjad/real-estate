@extends('frontend.layouts.layout')

@section('head')
<title>Services | Orion</title>
<meta name="description" content="Services | Orion">
<meta name="keywords" content="Services | Orion">
<style>
    li.hex-row {
     margin-top: -10vw;
}
li.hex-row:nth-child(2n) .hexagon {
  transform: translateX(50%) rotate(120deg);
}
 ul#hexagonContainer {
  margin: auto;
  list-style: none;
  padding: 0;
  margin-top: 10vw;
  }
 .hexagon {
  width: 30vw;
  background: transparent;
  height: 15vw;
  display: inline-block;
  transform: rotate(120deg);
  overflow: hidden;
  visibility: hidden;
  margin-bottom: 4vw;
  position: relative;
  }
 li.hex-row {
  white-space: nowrap;
  filter: drop-shadow(0.2vw 2vw 0.2vw #eeeeee);
  }
  .hexagon .hex-inner {
  width: 100%;
  height: 100%;
  background: rebeccapurple;
  transform: rotate(-60deg);
  overflow: hidden;
   position: relative;
  }
  .hexagon .hex-img {
   width: 100%;
   height: 100%;
   transform: rotate(-60deg);
   visibility: visible;
   box-shadow: 1px 0px 0px 0px;
   background-color: #2bb6aa;
  }
   .hexagon .hex-img:after {
   position: absolute;
   width: 100%;
   content: '';
   z-index: 1;
   height: 100%;
//background-image: url(https://farm9.staticflickr.com/8461/8048823381_0fbc2d8efb.jpg);
   background-position: center center;
   background-repeat: no-repeat;
   }
  .hex-img {
  visibility: hidden;
}

.text{
      position: absolute;
    //z-index: 1000;
    width: 100%;
    text-align: center;
}

background: radial-gradient(circle, transparent 0%, #2bb6aa 60%);

</style>
@endsection

@section('content')
<section class="header-image bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'>
    <img src="{{ asset('images/services-header.png') }}" />
  {{-- <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-title-name">
          <h1>Services</h1>
        </div>
        <ul class="page-breadcrumb">
          <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
          <li><span>Services</span> </li>
        </ul>
      </div>
    </div>
  </div> --}}
</section>
{{-- <section class="pt-50 pb-50">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
              <div class="section-title text-center">
                <h2 class="text-black">OUR SERVICES</h2>
                <p class="text-black">Our services include preliminary to detailed engineering designs, constructions services, project management and investment advisory for buildings, infrastructure and Oil and Gas related sectors</p>
              </div>
            </div>
        </div>
        <div class="row">
            <ul id="hexagonContainer">
                <!-- First row. -->
                <li class="hex-row">
                    @foreach ($servicesThree as $service)
                        <div class="hexagon">
                            <a data-toggle="modal" data-target="#exampleModalCenter" href="#" data-service="{{ $service->id }}" class="cricleButton">
                                <div class="hex-inner">
                                    <div class="hex-img" style="background-color:{{ $service->color }}!important">
                                        <div class="p-auto cricle-text">
                                            <h6 class="hexagon-text text-white text-center">{{ Str::of($service->title)->words(1.5, '...') }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </li>
                 <!-- Second row. -->
                <li class="hex-row">
                    @foreach ($servicesTwo as $two)
                        <div class="hexagon">
                            <a data-toggle="modal" data-target="#exampleModalCenter" href="#" data-service="{{ $two->id }}" class="cricleButton">
                                <div class="hex-inner">
                                    <div class="hex-img" style="background-color:{{ $two->color }}!important">
                                        <div class="p-auto cricle-text">
                                            <h6 class="hexagon-text text-white text-center">{{ Str::of($two->title)->words(1.5, '...') }}</h6>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </li>
            </ul>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
              <div class="section-title text-center">
                <p class="text-black">Our team has an extensive experience in design and construction of projects by maintaining relationships with many international clients and consultants. We possess a broad range of technical, management and leadership skills with the innate ability to lead through adversity to complete challenging projects.</p>
              </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="padding-right:0px !important">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-title" id="exampleModalLongTitle">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section class="blog gray-bg page-section-ptb">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12 text-center" style="background: #1E64B0;
            padding: 10px;">
                <h1 class="text-white">Our Services</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($services as $service)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-entry mb-50 slideInUp wow" style="box-shadow: 0px 0px 30px rgb(0 0 0 / 10%);">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $service->featured_image) }}" alt="">
                    </div>
                    <div class="blog-detail bg-primary">
                        <div class="entry-title mb-10">
                            <a class="text-white" href="{{ route('pages.service-details', $service->slug) }}">{{ $service->title }}</a>
                        </div>
                        <div class="entry-title blog-description mb-10">
                            {!! Str::of($service->description)->words(20, '...') !!}
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button button-border white" style="background:#f5793b;color:white;border: 2px solid #f5793b;" href="{{ route('pages.service-details', $service->slug) }}">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
@push('scripts')
    <script>
        $('.cricleButton').on('click', function(e){
            e.preventDefault();
            var service_id = this.getAttribute('data-service');
            var url = "{{ route('pages.service-details', [':id']) }}";
            url = url.replace(":id", service_id);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: url,
                dataType: 'json',
                success:function(data){
                    var image = `{{ asset('/storage/${data.featured_image}') }}`;
                    $('#exampleModalLongTitle').html('');
                  $('#exampleModalLongTitle').append(`
                    <div class="section-title mb-10">
                        <img style="width: -webkit-fill-available;" src="${image}" alt="${data.title}"/>
                        <h2 class="mt-2">${data.title}</h2>
                        <p>${data.description}</p>
                    </div>
                  `);
                }
            });
        });
    </script>
@endpush
