@extends('frontend.layouts.layout')

@section('head')
    <title>{{ $latestNew->meta_title }}</title>
    <meta name="description" content="{{ $latestNew->meta_description }}">
    <meta name="keywords" content="{{ $latestNew->meta_keywords }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ul class="page-breadcrumb">
                <li style="color:#1E64B0"><a style="color:#1E64B0" href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                        class="fa fa-angle-double-right"></i></li>
                <li style="color:#000000"><a style="color:#000000" href="{{ route('pages.latest-news') }}">
                        Latest News</a></li>
            </ul>
        </div>
    </div>
</div>
<section class="blog white-bg page-section-ptb" style="padding:10px 0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color fw-5">{{ $latestNew->title }}</h2>
                <p>{{ date('M d, Y', strtotime($latestNew->published_date)) }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $latestNew->body_image) }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                {!! $latestNew->description !!}
            </div>
        </div>
    </div>
</section>

@if (count($relatedLatestNews) > 0)
<section class="blog white-bg page-section-ptb p-0">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color fw-5">Related Latest News</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($relatedLatestNews as $relatedLatestNewsRecord)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-entry mb-50">
                        <div class="entry-image clearfix">
                            <img class="img-fluid" src="{{ asset('/storage/' . $relatedLatestNewsRecord->featured_image) }}" alt="">
                        </div>
                        <div class="blog-detail">
                            <div class="entry-title mb-10">
                                <a href="{{ route('pages.latest-news-details', $relatedLatestNewsRecord->slug) }}">{{ $relatedLatestNewsRecord->title }}</a>
                            </div>
                            <div class="entry-meta mb-10">
                                <ul>
                                    <li><a href="{{ route('pages.latest-news-details', $relatedLatestNewsRecord->slug) }}"><i class="fa fa-calendar-o"></i> {{ date('d M Y', strtotime($relatedLatestNewsRecord->published_date)) }}</a></li>
                                </ul>
                            </div>
                            <div class="entry-content">
                               {!! $relatedLatestNewsRecord->excerpt !!}
                            </div>
                            <div class="entry-share clearfix">
                                <div class="entry-button">
                                    <a class="button arrow" href="{{ route('pages.latest-news-details', $relatedLatestNewsRecord->slug) }}">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@endsection
