@extends('frontend.layouts.layout')

@section('head')
<title>Projects | Orion</title>
<meta name="description" content="Projects | Orion">
<meta name="keywords" content="Projects | Orion">
@endsection

@section('content')
<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
    style="background-image: url({{ asset('images/mufid-majnun-h1kOzS2sGAk-unsplash.jpg') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>Projects</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><span>Projects</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>

@if (count($cityProjects) > 0)
<section class="blog gray-bg page-section-ptb" style="padding:60px 0">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12 text-center" style="background: #1E64B0;
            padding: 10px;">
                <h1 class="text-white">Our Projects</h1>
            </div>
        </div>
        <div class="row">
            @foreach ($cityProjects as $project)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-entry mb-50 slideInUp wow" style="box-shadow: 0px 0px 30px rgb(0 0 0 / 10%);">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $project->featured_image) }}" alt="" style="height:240px">
                    </div>
                    <div class="blog-detail bg-primary">
                        <div class="entry-title mb-10">
                            <a class="text-white" href="{{ route('pages.project-details', $project->slug) }}">{{ $project->title }}</a>
                        </div>
                        <div class="entry-title blog-description mb-10" style="height:248px">
                            {!! Str::of($project->description)->words(33, '...') !!}
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button button-border white" style="background:#f5793b;color:white;border: 2px solid #f5793b;" href="{{ route('pages.project-details', $project->slug) }}">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@else
<h2 class="mt-5 mb-5 text-center">Projects Not Found</h2>
@endif
@endsection
