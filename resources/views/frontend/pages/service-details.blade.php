@extends('frontend.layouts.layout')

@section('head')
    <title>{{ $service->meta_title }}</title>
    <meta name="description" content="{{ $service->meta_description }}">
    <meta name="keywords" content="{{ $service->meta_keywords }}">
@endsection

@section('content')
{{-- <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ul class="page-breadcrumb">
                <li style="color:#1E64B0"><a style="color:#1E64B0" href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                        class="fa fa-angle-double-right"></i></li>
                <li style="color:#000000"><a style="color:#000000" href="{{ route('pages.blogs') }}">
                        Services</a></li>
            </ul>
        </div>
    </div>
</div> --}}
<section class="blog white-bg page-section-ptb" style="padding:20px 0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <h2 class="theme-color fw-5">{{ $service->title }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="blog-entry mb-10">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" style="height:500px;" src="{{ asset('/storage/' . $service->featured_image) }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-12 col-lg-12">
                <p>{!! $service->description !!}
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
