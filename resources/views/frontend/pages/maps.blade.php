@extends('frontend.layouts.layout')

@section('head')
<title>Maps | Orion</title>
<meta name="description" content="Maps | Orion">
<meta name="keywords" content="Maps | Orion">
@endsection

@section('content')
<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
    style="background-image: url({{ asset('images/login.jpg') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>Maps</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><span>Maps</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>

@if (count($maps) > 0)
<section class="blog white-bg page-section-ptb">
    <div class="container">
        <div class="row">
            @foreach ($maps as $map)
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-entry mb-50">
                    <div class="entry-image clearfix">
                        <img class="img-fluid" src="{{ asset('/storage/' . $map->map) }}" alt="">
                    </div>
                    <div class="blog-detail">
                        <div class="entry-title mb-10">
                            <a href="{{ route('pages.map-detail', $map->id) }}">{{ $map->title }}</a>
                        </div>
                        <div class="entry-share clearfix">
                            <div class="entry-button">
                                <a class="button arrow text-left" href="{{ route('pages.map-detail', $map->id) }}">Read
                                    More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
@endsection
