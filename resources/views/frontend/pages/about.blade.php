@extends('frontend.layouts.layout')

@section('head')
<title>About | Orion</title>
<meta name="description" content="About | Orion">
<meta name="keywords" content="About | Orion">
<style>
    #jarallax-container-0 div {
        background-position: 0% !important;
    }
</style>
@endsection

@section('content')
{{-- <section class="bg-overlay-black-60">
    <img src="{{ asset('images/about-us.jpg') }}" />
</section> --}}
{{-- <section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
    style="background-image: url({{ asset('images/about-us.jpg') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>About us</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                    <li><span>About us</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section> --}}

<section class="header-image bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'>
    <img src="{{ asset('images/about-header.jpg') }}" />

    {{-- <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>About us</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><span>About us</span> </li>
                </ul>
            </div>
        </div>
    </div> --}}
</section>

<section class="page-section-ptb" style="padding:40px 0">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 sm-mt-30">
                <div class="section-title">
                    <h2 class=" title-effect rc">WHO WE ARE</h2>
                    <p style="font-size:16px;text-align: justify;">Orion D&C is an International design and construction services provider that has intense experience in project management, multidisciplinary Engineering and construction services.
                        Our team has an extensive experience in design and construction of projects by maintaining relationships with international clients and consultants of various regions.
                        We have completed several successful projects in Saudi Arabia, UAE and Pakistan.
                        We possess a broad range of technical, personal effectiveness and leadership skills with the innate ability to lead through adversity to complete countless projects within budget and on schedule.
                    </p>
                </div>
                <p class="">We also have an office in London to facilitate our overseas clients which you can visit</p>

            </div>
            <div class="col-lg-6" id="vision">
                <img id="mission" class="img-fluid full-width mb-20" src="{{ asset('/images/side-pic.png') }}" alt="">
            </div>
        </div>
        <div class="row fadeInLeft wow">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="feature-text left-icon mt-50 xs-mt-20">
                    <div class="feature-icon">
                        <span class="ti-desktop theme-color" aria-hidden="true"></span>
                    </div>
                    <div class="feature-info">
                        <h5 class=" text-back rc">Our Vision</h5>
                        <p style="color:black !important;">To be recognized as a world-class engineering and construction company.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="feature-text left-icon mt-50 xs-mt-20">
                    <div class="feature-icon">
                        <span class="ti-server theme-color" aria-hidden="true"></span>
                    </div>
                    <div class="feature-info">
                        <h5 class=" text-back rc">Our Mission</h5>
                        <p style="color:black !important;text-align: justify;">As the construction industry undergoes a digital transformation, Orion D&C is working to become a next-generation design and construction services firm and committed to provide exceptional value and demonstrable results to clients.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-section-ptb" id="why-us" style="padding:20px 0;">
    <div class="container">
        <div class="col-md-12 mb-3" style="background: #1E64B0;
        padding: 10px;">
            <h1 class="text-center text-white">OBJECTIVE & WHY US?</h1>
        </div>
        <div class="row">
            <div class="col-lg-12 sm-mt-30 fadeInRight wow">
                <div class="section-title">
                    <h2>Objective</h2>
                    <ul style="margin-left:20px">
                        <li>To consistently strive to meet our clients' expectations to the best of our abilities</li>
                        <li>To deliver, maintain, and sustain high-quality services to clients</li>
                        <li>To achieve excellent customer relations by promptly attending to customers' needs and requirements.</li>
                        <li>To provide best-in-class design and services that are subjected to continuous improvement.</li>
                        <li>To collaborate and engage with other industry players in order to share information and experience.</li>
                        <li>Fulfill our social responsibilities by donating to the less fortunate, charitable organizations, and other causes as the need arises.</li>
                    </ul>
                </div>
                <div class="section-title">
                    <h2>Why us?</h2>
                    <p style="font-size:16px;text-align: justify;">
                        <b>Experienced Design Team</b><br>
                        Our design team has a broad selection of styles to choose from, including modern, classic, innovative, and cutting-edge design. They are well-versed in current planning rules, and their expertise and experience enable customers to make well-informed decision regarding project design. Their knowledge guarantees that each client can make better decisions regarding color schemes, textures, materials, and accessories, all of which contribute to a finished project that is unique, inventive, and of the highest quality.
                    </p>
                    <br>
                    <p style="font-size:16px;text-align: justify;">
                        <b>Passion</b><br>
                        Orion Design & Construction Services are passionate about our work and our strength lies in working as a team to engage with our clients and deliver first class customer service and the highest standards of excellence.
                    </p>
                    <br>
                    <p style="font-size:16px;text-align: justify;">
                        <b>Individualism</b><br>
                        Orion Design & Construction Services ensures that for each design job, options are provided, with the idea that each client's creativity and functionality is unique. Our goal is to create customized solutions that are within a specified budget.
                    </p>
                    <br>
                    <p style="font-size:16px;text-align: justify;">
                        <b>Professionalism</b><br>
                        We pride ourselves on being professional and accountable. We keep strong documentation and provide you with a real-time information on all project details, timelines, updates and invoicing.
                    </p>
                    <br>
                    <p style="font-size:16px;text-align: justify;">
                        <b>All-Inclusive Approach</b><br>
                        We make designing and building your new project a full-service, stress-free experience. Your time is valuable, so we’re here to handle everything from start to finish. By providing total project management, we have the tools and team to conceptualize, demo and build every detail of your project.
                    </p>
                </div>
            </div>
        </div>
</section>
<div class="header-image">
    <img src={{ asset('/images/about-below.png') }} />
</div>
@endsection
