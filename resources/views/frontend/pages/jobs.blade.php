@extends('frontend.layouts.layout')

@section('head')
<title>Jobs | Orion</title>
<meta name="description" content="Jobs | Orion">
<meta name="keywords" content="Jobs | Orion">
@endsection

@section('content')
<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}'
    style="background-image: url({{ asset('images/josue-isai-ramos-figueroa-qvBYnMuNJ9A-unsplash.jpg') }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-name">
                    <h1>Jobs</h1>
                </div>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('pages.home') }}"><i class="fa fa-home"></i> Home</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><a href="{{ route('pages.careers') }}"><i class="fa fa-home"></i> Careers</a> <i
                            class="fa fa-angle-double-right"></i></li>
                    <li><span>{{ $career->title }}</span> </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="page-section-ptb" style="padding:60px 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 sm-mt-30">
                <div class="section-title">
                    <h2 class="theme-color ">{{ $career->title }}
                    </h2>
                    {!! $career->description !!}
                    <div class="mt-3">
                        <div class="awsm-job-specifications-row">
                            <div class="awsm-job-specification-wrapper">
                                <div class="awsm-job-specification-item awsm-job-specification-job-category">
                                    <i class="fa fa-users"></i>
                                    <span class="awsm-job-specification-label"><strong>Job Category: </strong></span>
                                    <span class="awsm-job-specification-term">{{ $career->job_category }}</span>
                                </div>
                                <div class="awsm-job-specification-item awsm-job-specification-job-type">
                                    <i class="fa fa-suitcase"></i>
                                    <span class="awsm-job-specification-label"><strong>Job Type: </strong></span>
                                    <span class="awsm-job-specification-term">{{ $career->job_type }}</span>
                                </div>
                                <div class="awsm-job-specification-item awsm-job-specification-job-location">
                                    <i class="fa fa-map-marker"></i>
                                    <span class="awsm-job-specification-label"><strong>Job Location: </strong></span>
                                    <span class="awsm-job-specification-term">{{ $career->location }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6" style="box-shadow: 0 0 3px 0 rgb(0 0 0 / 30%);
            padding: 35px;">
                <div class="section-title">
                    @include('common.partials.flash')
                    <h4 class="theme-color mb-3">Apply for this position</h4>
                    <form action="{{ route('pages.careers-job-apply', $career->slug) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-12 mt-3">
                            <label>Full Name <span style="color:red"> *</span></label>
                            <input type="text" class="form-control" name="name" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Email <span style="color:red"> *</span></label>
                            <input type="email" class="form-control" name="email" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Phone <span style="color:red"> *</span></label>
                            <input type="text" class="form-control" name="phone" />
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>City <span style="color:red"> *</span></label>
                            <select class="form-control" name="city">
                                <option value="">Select City</option>
                                <option value="lahore">Lahore</option>
                                <option value="karachi">Karachi</option>
                                <option value="islamabad">Islamabad</option>

                            </select>
                        </div>
                        <div class="col-md-12 mt-3">
                            <label>Upload CV/Resume <span style="color:red"> *</span></label>
                            <input type="file" class="form-control" accept=".pdf,.docx" name="cv" />
                        </div>
                        <div class="col-md-12 mt-3 text-center">
                            <input type="submit" class="button button-gradient" value="submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
