{{-- <header id="header" class="header fancy">
    <div class="topbar">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 xs-mb-10">
          <div class="topbar-call text-center text-md-left">
            <ul>
              <li><i class="fa fa-envelope-o text-white"></i><a href="mailto:info@odcs.com.pk"> info@odcs.com.pk</a></li>
               <li><i class="fa fa-phone"></i> <a href="tel:+923348969000"> <span>+(92) 334 896 9000 </span> </a> </li>
            </ul>
          </div>
        </div>

        <div class="col-lg-6 col-md-6" style="text-align:end">
            <div class="social-icons social-border color-hover clearfix"><ul>
                <li class="social-facebook"><a href="https://www.facebook.com/oriondcs"><i class="fa fa-facebook"></i></a></li>
                <li class="social-linkedin"><a href="https://www.linkedin.com/company/oriondcs"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
        </div>
       </div>
    </div>
  </div>
  <div class="menu">
    <div class="container">
      <div class="row">
       <div class="col-lg-12 col-md-12">
         <nav id="menu" class="mega-menu">
          <section class="menu-list-items">
          <ul class="menu-logo">
              <li>
                  <a href="{{ route('pages.home') }}"><img id="logo_img" src="{{ asset('images/logo.png') }}" alt="logo"> </a>
              </li>
          </ul>
          <div class="menu-bar">
            <ul class="menu-links">
                <li>
                    <a href="{{ route('pages.home') }}">Home</a>
                </li>
                <li><a href="{{ route('pages.about') }}"> About <i class="fa fa-angle-down fa-indicator"></i></a>
                    <ul class="drop-down-multilevel">
                        <li><a href="{{ route('pages.about') }}">Why Us</a></li>
                        <li><a href="{{ route('pages.about') }}">Vision</a></li>
                        <li><a href="{{ route('pages.about') }}">Mission</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('pages.services') }}"> Our Services <i class="fa fa-angle-down fa-indicator"></i></a>
                    <ul class="drop-down-multilevel">
                        @foreach ($parentServices as $parentService)
                        <li><a href="{{ route('pages.services') }}">{{ $parentService->title }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{ route('pages.projects') }}">Projects</a>
                </li>
                <li>
                    <a href="{{ route('pages.careers') }}">Career</a>
                </li>
                <li>
                    <a href="{{ route('pages.blogs') }}">Blog</a>
                </li>
                <li>
                    <a href="{{ route('pages.contact-us') }}">Contact us</a>
                </li>
            </ul>
            <div class="search-cart">
                <div class="search">
                <a class="search-btn not_click" href="javascript:void(0);"></a>
                    <div class="search-box not-click">
                        <form action="search.html" method="get">
                            <input type="text"  class="not-click form-control" name="search" placeholder="Search.." value="" >
                            <button class="search-button" type="submit"> <i class="fa fa-search not-click"></i></button>
                        </form>
                    </div>
                </div>
            </div>
          </div>
         </section>
           </nav>
         </div>
       </div>
      </div>
     </div>
</header> --}}

<header id="header" class="header light">

    <!--=================================
     mega menu -->

    <div class="menu">
      <!-- menu start -->
       <nav id="menu" class="mega-menu">
        <!-- menu list items container -->
        <section class="menu-list-items">
         <div class="container">
          <div class="row">
           <div class="col-lg-12 col-md-12">
            <!-- menu logo -->
            <ul class="menu-logo">
                <li>
                    <a href="{{ route('pages.home') }}"><img id="logo_img" src="{{ asset('images/logo.png') }}" alt="logo"> </a>
                </li>
            </ul>
            <!-- menu links -->
            <div class="menu-bar">
                <ul class="menu-links">
                    <li>
                        <a href="{{ route('pages.home') }}">Home</a>
                    </li>
                    <li><a href="{{ route('pages.about') }}"> About <i class="fa fa-angle-down fa-indicator"></i></a>
                        <ul class="drop-down-multilevel">
                            <li><a href="{{ route('pages.about', ['#why-us']) }}">Why Us</a></li>
                            <li><a href="{{ route('pages.about', ['#vision']) }}">Vision</a></li>
                            <li><a href="{{ route('pages.about', ['#mission']) }}">Mission</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('pages.services') }}"> Our Services <i class="fa fa-angle-down fa-indicator"></i></a>
                        <ul class="drop-down-multilevel">
                            @foreach ($parentServices as $parentService)
                            <li><a href="{{ route('pages.service-details', $parentService->slug) }}">{{ $parentService->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="https://drive.google.com/file/d/146LVz8ALjATsWSu0kKDSOHYE9b7gSqU3/view" target="_blank">Portfolio</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.projects') }}">Projects</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.careers') }}">Career</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.blogs') }}">Blog</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.contact-us') }}">Contact us</a>
                    </li>
                </ul>
                <div class="search-cart">
                    <div class="search">
                    <a class="search-btn not_click" href="javascript:void(0);"></a>
                        <div class="search-box not-click">
                            <form action="search.html" method="get">
                                <input type="text"  class="not-click form-control" name="search" placeholder="Search.." value="" >
                                <button class="search-button" type="submit"> <i class="fa fa-search not-click"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           </div>
          </div>
         </div>
        </section>
       </nav>
      <!-- menu end -->
     </div>
</header>
