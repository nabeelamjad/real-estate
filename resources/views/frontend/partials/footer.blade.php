<section class="google-map black-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="map-icon">
                </div>
            </div>
        </div>
    </div>
    <div class="map-open">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3402.516150678098!2d74.39161361476732!3d31.482493581382123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391905e28b54e757%3A0x8d3d3e1f994e211f!2sD.H.A.%20Main%20Blvd%2C%20Lahore%2C%20Punjab%2C%20Pakistan!5e0!3m2!1sen!2s!4v1648067068503!5m2!1sen!2s" style="border:0; width: 100%; height: 300px;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</section>
{{-- <footer class="footer footer-topbar black-bg">
    <div class="copyright">
     <div class="container">
       <div class="row">
         <div class="col-xl-6 col-md-6">
         <img class="img-fluid" id="logo-footer" style="height:70px !important" src="{{ asset('images/logo-dark.jpeg') }}" alt="">
               <div class="footer-text">
                  <p> ©Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2022</span> <a href="#"> Orion </a> All Rights Reserved </p>
               </div>
           </div>
           <div class="col-xl-6 col-md-6">
           <div class="footer-social">
             <ul class="text-start text-md-end">
                <li class="list-inline-item"><a href="{{ route('pages.home') }}">Home </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.about') }}">About </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.projects') }}">Projects </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.services') }}">Services </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.careers') }}">Career </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.blogs') }}">Blog </a> &nbsp;&nbsp;&nbsp;|</li>
                <li class="list-inline-item"><a href="{{ route('pages.contact-us') }}">Contact </a> </li>

             </ul>
           </div>
           <div class="social-icons color-hover mt-20 float-end">
                <ul class="clearfix">
                 <li class="social-facebook"><a href="https://www.facebook.com/oriondcs"><i class="fa fa-facebook"></i></a></li>
                 <li class="social-linkedin"><a href="https://www.linkedin.com/company/oriondcs"><i class="fa fa-linkedin"></i> </a></li>
                </ul>
              </div>
         </div>
       </div>
     </div>
    </div>
</footer> --}}
<footer class="footer page-section-pt black-bg">
    <div class="container">
     <div class="row">
         <div class="col-lg-2 col-sm-6 sm-mb-30">
         <div class="footer-useful-link footer-hedding">
           <h6 class="text-white mb-30 mt-10 text-uppercase">Navigation</h6>
           <ul>
            <li><a href="{{ route('pages.home') }}">Home </a></li>
            <li><a href="{{ route('pages.about') }}">About </a></li>
            <li><a href="{{ route('pages.services') }}">Services </a></li>
            <li><a href="{{ route('pages.projects') }}">Projects </a></li>
            <li><a href="{{ route('pages.careers') }}">Career </a></li>
            <li><a href="{{ route('pages.blogs') }}">Blog </a></li>
            <li><a href="{{ route('pages.contact-us') }}">Contact Us</a> </li>
           </ul>
         </div>
       </div>
       <div class="col-lg-2 col-sm-6 sm-mb-30">
         <div class="footer-useful-link footer-hedding">
           <h6 class="text-white mb-30 mt-10 text-uppercase">Useful Link</h6>
           <ul>
             <li><a href="{{ route('pages.about') }}">Company Philosophy</a></li>
             <li><a href="{{ route('pages.about') }}">Corporate Culture</a></li>
             <li><a target="_blank" href="https://drive.google.com/file/d/146LVz8ALjATsWSu0kKDSOHYE9b7gSqU3/view">Portfolio</a></li>
             {{-- @forelse ($services as $service)
             <li><a href="{{ route('pages.services') }}">{{ $service->title }}</a></li>
             @empty

             @endforelse --}}
           </ul>
         </div>
       </div>
       <div class="col-lg-4 col-sm-6 xs-mb-30">
       <h6 class="text-white mb-30 mt-10 text-uppercase">Contact Us</h6>
       <ul class="addresss-info">
           <li><i class="fa fa-map-marker"></i> <p>Head office: 11G, Commercial, Main Boulevard, DHA Phase 1, Lahore, Pakistan</p> </li>
           <li><i class="fa fa-map-marker"></i> <p>Regional Office: 160 Kemp House, City Road London ECIV 2NX, United Kingdom</p> </li>
           <li><i class="fa fa-phone"></i> <a href="tel:923348969000"> <span>+(92) 334 896 9000 </span> </a> </li>
           <li><i class="fa fa-envelope-o"></i><a href="mailto:info@odcs.com.pk">Email: info@odcs.com.pk</a></li>
         </ul>
       </div>
       <div class="col-lg-4 col-sm-6">
         <h6 class="text-white mb-30 mt-10 text-uppercase">Subscribe to Our Newsletter</h6>
           <p>Sign Up to our Newsletter to get the latest news and offers.</p>
            <div class="footer-Newsletter">
             <div id="mc_embed_signup_scroll">
               <form action="php/mailchimp-action.php" method="POST" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
                <div id="msg"> </div>
                 <div id="mc_embed_signup_scroll_2">
                   <input id="mce-EMAIL" class="form-control" type="text" placeholder="Email address" name="email1" value="">
                 </div>
                 <div id="mce-responses" class="clear">
                   <div class="response" id="mce-error-response" style="display:none"></div>
                   <div class="response" id="mce-success-response" style="display:none"></div>
                   </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                   <div style="position: absolute; left: -5000px;" aria-hidden="true">
                       <input type="text" name="b_b7ef45306f8b17781aa5ae58a_6b09f39a55" tabindex="-1" value="">
                   </div>
                   <div class="clear">
                     <button type="submit" name="submitbtn" id="mc-embedded-subscribe" class="button button-border mt-20 form-button">  Get notified </button>
                   </div>
                 </form>
               </div>
               </div>
            </div>
          </div>
         <div class="footer-widget mt-20">
           <div class="row">
             <div class="col-lg-6 col-md-6 xs-mb-20">
                <p> ©Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="{{ route('pages.home') }}"> Orion </a> All Rights Reserved </p>
             </div>
             <div class="col-lg-6 col-md-6">
               <div class="social-icons color-hover float-left float-md-right pt-10">
                <ul>
                <li class="social-facebook"><a href="https://www.facebook.com/oriondcs"><i class="fa fa-facebook"></i></a></li>
                <li class="social-linkedin"><a href="https://www.linkedin.com/company/oriondcs"><i class="fa fa-linkedin"></i> </a></li>
                <li class="social-twitter"><a href="https://www.facebook.com/oriondcs"><i class="fa fa-twitter"></i></a></li>
                <li class="social-instagram"><a href="https://www.facebook.com/oriondcs"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
             </div>
           </div>
         </div>
     </div>
   </footer>
