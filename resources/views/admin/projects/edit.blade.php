@extends('admin.layouts.layout')
@section('content')
@include('admin.partials.sidebar')

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Projects</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active"><a href="{{ route('projects.index') }}">Projects</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ul>
                </div>
            </div>
        </div>
        @include('common.partials.flash')

        <div class="row">
            <div class="col-md-12">
                <div class="card card-table">
                    <div class="card-header">
                        <h4 class="card-title">Edit Project - {{ $project->title }}</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('projects.update', $project->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row form-row">
                                <div class="col-6 col-md-12">
                                    <div class="form-group">
                                        <div class="change-avatar">
                                            <div class="profile-img" style="margin-bottom:20px;">
                                                @if($project->featured_image)
                                                    <img height="150" src="{{ asset('/storage/' . $project->featured_image) }}" alt="User featured_image">
                                                @endif
                                            </div>
                                            <div class="upload-img">
                                                <div class="change-photo-btn">
                                                    <span class="@error('featured_image') text-danger @enderror"><i class="fa fa-upload"></i> Upload Featured Image <span style="color:red">*</span> </span>
                                                    <input type="file" class="@error('featured_image') text-danger @enderror upload form-control" name="featured_image">
                                                </div>
                                                <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="@error('title') text-danger @enderror">Title <span style="color:red">*</span> </label>
                                        <input type="text" class="@error('title') border-danger @enderror form-control" name="title" value="{{ old('title', $project->title) }}" required >
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="@error('address') text-danger @enderror">Address <span style="color:red">*</span> </label>
                                        <input type="text" class="@error('address') border-danger @enderror form-control" value="{{ old('address', $project->address) }}" name="address" required >
                                    </div>
                                </div>
                                <div class="col-6 col-md-12">
                                    <div class="form-group">
                                        <div class="change-avatar">
                                            @if ($project->application_form)
                                                <div class="profile-img" style="margin-bottom:20px;">
                                                    @foreach (explode(',', $project->application_form) as $form)
                                                        @if (explode('.', $form)[1] == 'jpg' || explode('.', $form)[1] == 'png' || explode('.', $form)[1] == 'jpeg' || explode('.', $form)[1] == 'gif')
                                                            <a class="btn btn-sm bg-primary-light" title="{{ $form }}" href="{{ asset('/storage/' . $form) }}" target="_blank"><i class="fe fe-eye"></i>View  Image</a>
                                                            <a href="{{ route('projects.delete-form', [$project->slug, $form]) }}" class="btn btn-danger text-white"
                                                                onclick="return confirm('Are you sure you want to delete this image?')"
                                                                style="margin-left:0px; top: 0; left: 0;margin-top: 0px;"> &times; </a>
                                                        @else
                                                            @if (explode('.', $form)[1] == 'docx')
                                                                <a class="btn btn-sm bg-primary-light" href="https://view.officeapps.live.com/op/view.aspx?src={{url(asset('/storage/' . $form ))}}" target="_blank"><i class="fe fe-eye"></i> View Docx</a>
                                                                <a href="{{ route('projects.delete-form', [$project->slug, $form]) }}" class="btn btn-danger text-white"
                                                                    onclick="return confirm('Are you sure you want to delete this form?')"
                                                                    style="margin-left:0px; top: 0; left: 0;margin-top: 0px;"> &times; </a>
                                                            @else
                                                                <a class="btn btn-sm bg-primary-light" title="{{ $form }}" href="{{ asset('/storage/' . $form) }}" target="_blank"><i class="fe fe-eye"></i> View Pdf</a>
                                                                <a href="{{ route('projects.delete-form', [$project->slug, $form]) }}" class="btn btn-danger text-white"
                                                                    onclick="return confirm('Are you sure you want to delete this image?')"
                                                                    style="margin-left:0px; top: 0; left: 0;margin-top: 0px;"> &times; </a>
                                                            @endif
                                                        @endif
                                                @endforeach
                                                </div>
                                            @endif
                                            <div class="upload-img">
                                                <div class="change-photo-btn">
                                                    <span class="@error('application_form') text-danger @enderror"><i class="fa fa-upload"></i> Upload Application Form</span>
                                                    <input accept=".pdf,.docx" type="file" class="@error('application_form') text-danger @enderror upload form-control" name="application_form[]" multiple>
                                                </div>
                                                <small class="form-text text-muted">Allowed Pdf, Word. Max size of 2MB</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 col-md-12">
                                    <div class="form-group">
                                        <div class="change-avatar">
                                            <div class="profile-img" style="margin-bottom:20px;">
                                                @foreach (explode(',', $project->body_images) as $image)
                                                    <img height="150" width="100" class="mr-2" src="{{ asset('/storage/' . $image ) }}" alt="User Image">
                                                    <a href="{{ route('projects.delete-image', [$project->slug, $image ? $image : 'empty']) }}" class="btn btn-danger text-white"
                                                        onclick="return confirm('Are you sure you want to delete this image?')"
                                                        style="margin-left:-45px; top: 0; left: 0;margin-top: -110px;"> &times; </a>
                                                @endforeach
                                            </div>
                                            <div class="upload-img">
                                                <div class="change-photo-btn">
                                                    <span class="@error('body_images') text-danger @enderror"><i class="fa fa-upload"></i> Upload Body Images <span style="color:red">*</span> </span>
                                                    <input type="file" class="@error('body_images') text-danger @enderror upload form-control" name="body_images[]" multiple>
                                                </div>
                                                <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label class="@error('location') text-danger @enderror">Location Iframe<span style="color:red">*</span> </label>
                                        <input type="text" value="{{ old('location', $project->location) }}" class="@error('location') text-danger @enderror form-control" name="location" />
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label class="@error('description') text-danger @enderror">Description <span style="color:red">*</span> </label>
                                        <textarea class="@error('description') text-danger @enderror tinymce form-control" name="description">{{ old('description',$project->description) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <label class="@error('meta_title') text-danger @enderror">Meta Title</label>
                                        <input class="@error('meta_title') body-danger @enderror form-control" value="{{ old('meta_title', $project->meta_title) }}" name="meta_title">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="@error('meta_keywords') text-danger @enderror">Meta Keywords</label>
                                        <textarea class="@error('meta_keywords') text-danger @enderror form-control" name="meta_keywords">{{ old('meta_keywords', $project->meta_keywords) }}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="@error('meta_description') text-danger @enderror">Meta Description</label>
                                        <textarea class="@error('meta_description') text-danger @enderror form-control" name="meta_description">{{ old('meta_description', $project->meta_description) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="submit-section">
                                <button type="submit" class="btn btn-primary submit-btn">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="margin-left: 400px">
@include('admin.partials.footer')
</div>
@endsection
