{{-- @extends('admin.layouts.layout')

@section('heading')
    <h4>
        <a href="{{ route('admin.pages.home') }}">
            <i class="icon-arrow-left52 mr-2"></i>
        </a>
        <span class="font-weight-semibold">Home - Contact Enquiries</span> - View All
    </h4>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb">
        <a href="{{ route('admin.pages.home') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
        <span class="breadcrumb-item">Contact Enquiries</span>
        <span class="breadcrumb-item active">View All</span>
    </div>
@endsection

@section('content')
    @include('admin.partials.header')
    <div class="content">
        @include('common.partials.flash')
        <div class="card has-table">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">All Contact Enquiries</h5>
            </div>

            <table class="table datatable-pagination">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Comment</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($contactEnquiries as $key =>  $contactEnquiry)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $contactEnquiry->name }}</td>
                        <td>{{ $contactEnquiry->email }}</td>
                        <td>{{ $contactEnquiry->phone }}</td>
                        <td style="width:300px"> {{ $contactEnquiry->message }} </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <form action="{{route('contact-enquiries.destroy',$contactEnquiry->id) }}"   method="POST">
                                    @csrf
                                    @method('Delete')
                                    <button type="submit" class=" list-icons-item text-danger-600" onclick="return confirm('Are you sure you want to delete this Contact Inquiry?')" style="background-color: Transparent; border: none;cursor:pointer;"><i class="icon-trash"></i></button>
                                </form>
                            </div>
                        </td>
                        <td style="display: none"></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6">
                            <div class="alert alert-info text-center">
                                No Contact Enquiries So Far
                            </div>
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
<div style="margin-left: 400px">
@include('admin.partials.footer')
</div>  @endsection

@push('scripts')
    <script src="{{asset('backend/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('backend/js/demo_pages/datatables_basic.js')}}"></script>
@endpush --}}

@extends('admin.layouts.layout')

@section('content')
@include('admin.partials.sidebar')

<div class="page-wrapper">

    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Projects</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                        <li class="breadcrumb-item active"><a href="{{ route('projects.index') }}">Projects</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @include('common.partials.flash')

        <!-- /Page Header -->

        <div class="row">
            <div class="col-md-12">

                <!-- Recent Orders -->
                <div class="card card-table">
                    <div class="card-header" style="display: flex">
                        <div class="col-sm-10 col" style="margin-top:10px;">
                            <h4 class="card-title">Projects List</h4>
                        </div>
                        <div class="col-sm-2 col">
                            <a href="{{ route('projects.create') }}" class="btn btn-primary float-right mt-2">Add</a>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatable table table-stripped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Comment</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($contactEnquiries as $key => $contactEnquiry)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $contactEnquiry->name }}</td>
                                        <td>{{ $contactEnquiry->email }}</td>
                                        <td>{{ $contactEnquiry->phone }}</td>
                                        <td style="width:300px"> {{ $contactEnquiry->message }} </td>
                                        <td class="text-center">
                                            <div class="list-icons">
                                                <form action="{{route('contact-enquiries.destroy',$contactEnquiry->id) }}"   method="POST">
                                                    @csrf
                                                    @method('Delete')
                                                    <button type="submit" class=" list-icons-item text-danger-600" onclick="return confirm('Are you sure you want to delete this Contact Inquiry?')" style="background-color: Transparent; border: none;cursor:pointer;"><i class="fe fe-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">
                                                <div class="alert alert-info text-center">
                                                    No Projects Added So Far
                                                    <br>
                                                    <a href="{{ route('projects.create') }}" class="mt-2 btn btn-primary">
                                                        Add A New Project
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin-left: 400px">
@include('admin.partials.footer')
</div>
@endsection
@push('script')
    <script>
        $('.activeInactive').change(function(status){
            var user_id = this.getAttribute('data-status-id');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'get',
                url: "/admin/"+ user_id +"/set-status/"+status.target.value,
                dataType: 'json',
                success:function(data){
                    if(data == 'done')
                    {
                        $('#status').bootstrapToggle('on');
                    }
                }
            });
        });
    </script>
@endpush
