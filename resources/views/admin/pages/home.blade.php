@extends('admin.layouts.layout')
@section('content')
@include('admin.partials.sidebar')

<div class="page-wrapper">

    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    @if (auth()->user()->is_master == 1)
                        <h3 class="page-title">Welcome Super Admin</h3>
                    @else
                        <h3 class="page-title">Welcome Administrator</h3>
                    @endif
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                </div>
            </div>
        </div>
        @include('common.partials.flash')

        <!-- /Page Header -->

        <div class="row">
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fe fe-users"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ $projects }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted"><a href="{{ route('projects.index') }}">Projects</a></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width:{{ ($projects/100) * 100 }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fe fe-users"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ $careers }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted"><a href="{{ route('careers.index') }}">Careers</a></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width:{{ ($careers/100) * 100 }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fe fe-users"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ $blogs }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted"><a href="{{ route('blogs.index') }}">Blogs</a></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width:{{ ($blogs/100) * 100 }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fe fe-users"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ $services }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted"><a href="{{ route('services.index') }}">Services</a></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width:{{ ($services/100) * 100 }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
                            <span class="dash-widget-icon text-success">
                                <i class="fe fe-users"></i>
                            </span>
                            <div class="dash-count">
                                <h3>{{ $testimonials }}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">

                            <h6 class="text-muted"><a href="{{ route('testimonials.index') }}">Testimonials</a></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" style="width:{{ ($testimonials/100) * 100 }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card card-chart">
                    <div class="card-header">
                        <h4 class="card-title">Novels Graph</h4>
                    </div>
                    <div class="card-body">
                        <div id="chartContainer" style="width: 100%; height: 400px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.partials.footer')
</div>
@endsection
@push('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
    var data = google.visualization.arrayToDataTable([
    ['Task','Documents'],
    ['Published', 43],
    ['Un-Publish', 76],
    ['Completed', 12],
    ['Ongoing', 88],
]);

    // Optional; add a title and set the width and height of the chart
    var options = {
        colors: ["#dc3545",'#fd7e14',"#2bb6aa", "#007bff"],
    };

    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('chartContainer'));
    chart.draw(data, options);
}
</script>
@endpush
