<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clear-cache', function () {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    return "<h1>Cache Cleared</h1>";
});

Auth::routes();

Route::group(["namespace" => "Frontend"], function () {
    Route::get('/', 'PageController@index')->name('pages.home');
    Route::get('/about/who-we-are', 'PageController@about')->name('pages.about');

    Route::get('/services', 'PageController@service')->name('pages.services');
    Route::get('/services/{id}', 'PageController@serviceDetail')->name('pages.service-details');
    Route::get('/service/{service:slug}', 'PageController@serviceDetails')->name('pages.service-details');


    Route::get('/blogs', 'PageController@blog')->name('pages.blogs');
    Route::get('/blog/{blog:slug}', 'PageController@blogDetails')->name('pages.blog-details');

    Route::get('/projects', 'PageController@project')->name('pages.projects');
    Route::get('/projects/{project:slug}', 'PageController@projectDetails')->name('pages.project-details');

    Route::get('/careers', 'PageController@careers')->name('pages.careers');
    Route::get('/careers/{career:slug}/job', 'PageController@careerJob')->name('pages.careers-job');
    Route::post('/careers/{career:slug}/job/apply', 'PageController@careerJobApply')->name('pages.careers-job-apply');
    Route::post('/careers/job/apply', 'PageController@careerJobTeam')->name('pages.careers-job-team');

    Route::get('/contact-us', 'PageController@contactUs')->name('pages.contact-us');
    Route::post('/contact-enquiry', 'PageController@contactEnquiry')->name('pages.contact-enquiry');

    Route::post('/contact/form-submit', 'PageController@contactSubmit')->name('pages.contact-form-submit');

});

Route::get('/create-symlink', function () {
    $projectFolder = base_path() . '/../';
    // The file that you want to create a symlink of
    $source = $projectFolder . "/dwehidIFreyu/storage/app/public";
    // The path where you want to create the symlink of the above
    $destination = $projectFolder . "/storage";

    if (file_exists($destination)) {
        if (is_link($destination)) {
            return "<h1>Symlink already exists</h1>";
        }
    } else {
        symlink($source, $destination);
        return "<h1>Symlink created successfully</h1>";
    }
});

Route::get('/remove-symlink', function () {
    $projectFolder = base_path() . '/../';
    $destination = $projectFolder . "/storage";
    if (file_exists($destination)) {
        if (is_link($destination)) {
            unlink($destination);
            return "<h1>Removed symlink</h1>";
        }
    } else {
        return "<h1>Symlink does not exist</h1>";
    }
});
